//
// Created by Ignas on 3/17/2020.
//

#ifndef UNTITLED1_SYMBOLTABLE_H
#define UNTITLED1_SYMBOLTABLE_H

#include <vector>
#include "Symbol.h"


using namespace std;
class SymbolTable {

public:
    vector<Symbol*> table;
    int varCount, argCount, staticCount, fieldCount;

    SymbolTable(){
        varCount = 0;
        argCount = 0;
        staticCount = 0;
        fieldCount = 0;
    }

    void AddSymbol(string name, Symbol::SymbolKind kind, string type);
    // returns true if symbol exists and false otherwise
    bool FindSymbol(string name);
    // returns pointer to symbol if it exists and null pointer if it doesn't
    Symbol* getSymbol(string name);

};


#endif //UNTITLED1_SYMBOLTABLE_H
