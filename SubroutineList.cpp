//
// Created by ignas on 2020-05-09.
//

#include "SubroutineList.h"
#include <iostream>
using namespace std;

void SubroutineList::warning(string message, Token* t) {
    cout<<endl<<"Warning in file "<< fileName<<endl;
    cout<<"Line "<<t->lineNumber<<" at or near "<<t->Lexeme<<endl;
    cout<<"Message: "<<message<<endl;
}

void SubroutineList::addIdentifier(string name, int args, Token* t){
    if(identifiers.find(name) != identifiers.end()){
        if(identifiers[name].first != args) {
            warning("Given number of arguments does not match the function", identifiers[name].second);
        }
    }
    else
        identifiers[name] = pair<int,Token*>(args,t);
}

bool SubroutineList::markIdentifier(string name, int args, string type){
    if(identifiers.find(name) != identifiers.end()){
        if(identifiers[name].first != args) {
            warning("Given number of arguments does not match the function", identifiers[name].second);
            return false;
        }
    }
    identifiers[name] = pair<int,Token*>(args,NULL);
    return true;
}

void SubroutineList::isEverytingDeclared(){

    for(auto id : identifiers){
        if(id.second.second != NULL)
            warning("Variable or Subroutine was used but never declared",id.second.second);
    }
}