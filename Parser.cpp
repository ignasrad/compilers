//
// Created by sc18ir on 03/03/20.
//

#include "Parser.h"
#include <unordered_map>

void Parser::init(string fileName) {
    lexer = new Lexer();

    if(!lexer->init(fileName)){
        cout<<"Unable to init parser"<<endl;
        return;
    }
    createPredefinedLibraries();
    cout<<"Parser initialized"<<endl;
    while(lexer->peekNextToken()->Type != Token().Eof){
        classDeclar();
        cout<<endl<<endl;
        writeCode(fileName);
    }
    checkIfIdentifiersMatch();
}
void Parser::error(string message, Token* t) {
    cout<<"Error in file "<< lexer->getFilename()<<endl;
    cout<<"Line "<<t->lineNumber<<" at or near "<<t->Lexeme<<endl;
    cout<<"Message: "<<message<<endl;
    exit(0);
}
void Parser::warning(string message, Token* t) {
    cout<<"Warning in file "<< lexer->getFilename()<<endl;
    cout<<"Line "<<t->lineNumber<<" at or near "<<t->Lexeme<<endl;
    cout<<"Message: "<<message<<endl;
}
void Parser::printOk(Token* t) {
//    cout<<t->Lexeme<<": OK"<<endl;
}
void Parser::SymbolAlreadyDeclared(Token* t){
    if(symbolTables.back()->FindSymbol(t->Lexeme)){
        error("Variable was already declared", t);
    }
}
void Parser::SymbolNotDeclared(Token *t){
    bool found = false;
    for(int i=0; i<symbolTables.size(); i++){
        if(symbolTables.at(i)->FindSymbol(t->Lexeme)){
            found = true;
        }
    }
    if(!found){
        error("Variable wasn't declared", t);
    }
}
Symbol* Parser::getSymbol(Token *t){
    for(int i=symbolTables.size()-1; i>=0; i--){
        if(symbolTables.at(i)->FindSymbol(t->Lexeme)){
            return symbolTables.at(i)->getSymbol(t->Lexeme);
        }
    }
    return NULL;
}
Symbol* Parser::getSymbolByName(string s){
    for(int i=0; i<symbolTables.size(); i++){
        if(symbolTables.at(i)->FindSymbol(s)){
            return symbolTables.at(i)->getSymbol(s);
        }
    }
    return NULL;
}
void Parser::symbolInitialized(Token *t){
    for(int i=0; i<symbolTables.size(); i++){
        if(symbolTables.at(i)->FindSymbol(t->Lexeme)){
            if(!symbolTables.at(i)->getSymbol(t->Lexeme)->initialized) {
                warning("Identifier used without initializing ", t);
            }
            else return;
        }
    }
}

// Identifiers Table
void Parser::addIdentifier(string className, string idName, int args, Token *t){
    for(string lib : defaultLibraries){
        if(lib == className)
            return;
    }

    if(identifiersToCheck.find(className) == identifiersToCheck.end()){
        identifiersToCheck[className] = new SubroutineList(className,lexer->getFilename());
    }
    identifiersToCheck[className]->addIdentifier(idName,args,t);
}
void Parser::markIdentifier(string idName, int args, string type){
    if(identifiersToCheck.find(className) == identifiersToCheck.end()){
        return;
    }
    identifiersToCheck[className]->markIdentifier(idName,args,type);
}
void Parser::checkIfIdentifiersMatch(){
    for(auto id : identifiersToCheck){
        id.second->isEverytingDeclared();
        delete(id.second);
    }
}
void Parser::createPredefinedLibraries(){
    string type = "";
    string math = "Math";
    string memory = "Memory";
    string screen = "Screen";
    //    Class Math
    identifiersToCheck[math] = new SubroutineList(math,math);
    identifiersToCheck[math]->markIdentifier("abs",1,type);
    identifiersToCheck[math]->markIdentifier("multiply",2,type);
    identifiersToCheck[math]->markIdentifier("divide",2,type);
    identifiersToCheck[math]->markIdentifier("min",2,type);
    identifiersToCheck[math]->markIdentifier("max",2,type);
    identifiersToCheck[math]->markIdentifier("sqrt",1,type);

    //     Class Memory
    identifiersToCheck[memory] = new SubroutineList(memory,memory);
    identifiersToCheck[memory]->markIdentifier("peek",1,type);
    identifiersToCheck[memory]->markIdentifier("poke",2,type);
    identifiersToCheck[memory]->markIdentifier("alloc",1,type);
    identifiersToCheck[memory]->markIdentifier("deAlloc",1,type);

    //     Screen Memory
    identifiersToCheck[screen] = new SubroutineList(screen,screen);
    identifiersToCheck[screen]->markIdentifier("clearScreen",0,type);


}

// Code generation
void Parser::addCodeSeg(string seg){
    code.push_back(seg);
}
void Parser::writeCode(string filePath){
    ofstream out;
    out.open (filePath + "/" + className + ".vm");

    for(auto s : code){
        cout<<s<<endl;
    }
    cout<<"Written to: "<<filePath + "/" + className + ".vm"<<endl;
    out.close();
    code.clear();
}

void Parser::classDeclar() {
    Token *t = lexer->getNextToken();

    if(t->Lexeme == "class"){
        symbolTables.push_back(new SymbolTable());
        printOk(t);
        t = lexer->getNextToken();
        if(t->Type == Token().Identifier){
            className = t->Lexeme;
            symbolTables.back()->AddSymbol(t->Lexeme,Symbol().classKind,"class");
            printOk(t);
            t = lexer->getNextToken();
            if(t->Lexeme == "{"){
                printOk(t);
                t = lexer->peekNextToken();
                while(t->Lexeme != "}"){
                    if(t->Type == Token().Eof){
                        error("Unexpected End of file is reached", t);
                    }
                    memberDeclar();
                    t = lexer->peekNextToken();
                }
                lexer->getNextToken();
                printOk(t);
            }
            else{
                error("{ is expected after class declaration", t);
            }
        }
        else{
            error("Class identifier is expected", t);
        }
    }
    else{
        error("The program should start with class declaration", t);
    }
}

void Parser::memberDeclar() {
    Token *t = lexer->peekNextToken();
    if(t->Lexeme == "static" || t->Lexeme == "field"){
        classVarDeclar();
    }
    else if(t->Lexeme == "constructor" || t->Lexeme == "function" || t->Lexeme == "method"){
        subroutineDeclar();
    }
    else{
        error("Either class or subroutine declaration expected",t);
    }
}

void Parser::classVarDeclar(){
    Token *t = lexer->peekNextToken();

    if(t->Lexeme == "static" || t->Lexeme == "field"){
        string kind = t->Lexeme;
        lexer->getNextToken();
        printOk(t);

        //peeking the type for new entry to the symbol
        string symbolType = lexer->peekNextToken()->Lexeme;
        type();
        t = lexer->getNextToken();
        if(t->Type == Token().Identifier){
            printOk(t);

            // Checking if it was used in previous files and matching it
            markIdentifier(t->Lexeme,-1,symbolType);

            //adding static or a field to a class symbol table
            SymbolAlreadyDeclared(t);
            if(kind == "static"){
                symbolTables.at(symbolTables.size() - 1)->AddSymbol(t->Lexeme, Symbol().stat, symbolType);
            }
            else{
                fieldCount++;
                symbolTables.at(symbolTables.size() - 1)->AddSymbol(t->Lexeme, Symbol().field, symbolType);
            }
            t = lexer->getNextToken();
            while(t->Lexeme == ","){
                printOk(t);
                t = lexer->getNextToken();
                if(t->Type == Token().Identifier){

                    // Checking if it was used in previous files and matching it
                    markIdentifier(t->Lexeme,-1,symbolType);

                    //adding static or a field to a class symbol table
                    SymbolAlreadyDeclared(t);
                    if(kind == "static"){
                        symbolTables.at(symbolTables.size() - 1)->AddSymbol(t->Lexeme, Symbol().stat, symbolType);
                    }
                    else{
                        fieldCount++;
                        symbolTables.at(symbolTables.size() - 1)->AddSymbol(t->Lexeme, Symbol().field, symbolType);
                    }
                    printOk(t);
                    t = lexer->getNextToken();
                }
                else{
                    error("Var declaration ends with , ", t);
                }
            }
        }
        if(t->Lexeme == ";"){
            printOk(t);
        }
        else{
            error("Missing ; symbol", t);
        }
    }
}

void Parser::type(){
    Token *t = lexer->getNextToken();

    if(t->Lexeme == "int" ||t->Lexeme == "boolean" || t->Lexeme == "char" || t->Type == Token().Identifier){
        printOk(t);
    }
    else{
        error("Variable type expected",t);
    }
}

void Parser::subroutineDeclar(){
    bool function = false;
    Token *t = lexer->getNextToken();
    if(t->Lexeme == "function" || t->Lexeme == "constructor" || t->Lexeme == "method"){
        Token * subroutine = t;
        //the last element in code fragment pointer
        //used when counting local vars
        int back = -1;
        if(t->Lexeme == "function")
            function = true;
        t = lexer->peekNextToken();

        //  setting the return type of the subroutine
        string symbolType = t->Lexeme;

        //create new symbol table
        SymbolTable *subTable = new SymbolTable();
        subTable->AddSymbol("this", Symbol().subroutine,t->Lexeme);
        symbolTables.push_back(subTable);
        if(t->Lexeme == "void"){
            printOk(t);
            lexer->getNextToken();
        }
        else{
            type();
        }
        t = lexer->getNextToken();
        if(t->Type == Token().Identifier){
            code.push_back("function " + className + "." + t->Lexeme);
            back = code.size()-1;
            if(subroutine->Lexeme == "constructor"){
                code.push_back("push constant " + to_string(fieldCount));
                code.push_back("call Memory.alloc 1");
                code.push_back("pop pointer 0");
            }
            if(subroutine->Lexeme == "method"){
                code.push_back("push argument 0");
                code.push_back("pop pointer 0");
            }

                printOk(t);
            string idName = t->Lexeme;
            t = lexer->getNextToken();
            if(t->Lexeme == "("){
                printOk(t);
                paramList();
            }
            else{
                error("( expected",t);
            }
            t = lexer->getNextToken();
            if(t->Lexeme == ")"){
                // Checking if subroutine was used in previous files and matching it
                markIdentifier(idName,subTable->getSymbol("this")->arguments,symbolType);

                printOk(t);
                subroutineBody();
                if(back > -1)
                    code.at(back) = code.at(back) + " " + to_string(localVariablesCount);
                if(function && !subTable->FindSymbol("return")){
                    warning("Function must always return a value",lexer->getNextToken());
                }

                delete(subTable);
                symbolTables.pop_back();
            }
            else{
                error(") expected",t);
            }
        }
        else{
            error("Identifier expected", t);
        }
    }
    else{
        error("Subroutine declaration expected", t);
    }
}

void Parser::paramList() {
    int count = 0;

    Token *t = lexer->peekNextToken();
    if(t->Lexeme == ")"){
        return;
    }

    if(t->Lexeme == "int" || t->Lexeme == "char" || t->Lexeme == "boolean" || t->Type == Token().Identifier){
        string argumentType = t->Lexeme;
        type();
        ++count;
        t = lexer->getNextToken();
        if(t->Type == Token().Identifier){
            SymbolAlreadyDeclared(t);
            symbolTables.at(symbolTables.size() - 1)->AddSymbol(t->Lexeme, Symbol().argument, argumentType);
            printOk(t);

            // marking an identifier as initialized
            Symbol* s = getSymbol(t);
            if(s != NULL)
                s->initialized = true;
        }
        else{
            error("Identifier expected ",t);
        }

        // might be more that one var for a function
        t = lexer->peekNextToken();
        while(t->Lexeme == ","){
            ++count;
            lexer->getNextToken();
            argumentType = lexer->peekNextToken()->Lexeme;
            type();
            t = lexer->getNextToken();
            if(t->Type == Token().Identifier){
                SymbolAlreadyDeclared(t);
                symbolTables.at(symbolTables.size() - 1)->AddSymbol(t->Lexeme, Symbol().argument, argumentType);
                printOk(t);

                // marking an identifier as initialized
                Symbol* s = getSymbol(t);
                if(s != NULL)
                    s->initialized = true;
            }
            else{
                error("Identifier expected ",t);
            }
            t = lexer->peekNextToken();
        }
    }
    else{
        printOk(t);
    }
    //    setting the number of arguments for the function
    symbolTables.back()->getSymbol("this")->arguments = count;
}

void Parser::subroutineBody() {
    Token *t = lexer->getNextToken();
    SymbolTable* subTable = symbolTables.back();
    localVariablesCount = 0;
    if(t->Lexeme == "{"){
        printOk(t);
        while(t->Lexeme != "}"){
            if(subTable->FindSymbol("return")){
                warning("This part of code will never be reached!", lexer->getNextToken());
            }
            statement();
            t = lexer->peekNextToken();
        }
        if(t->Lexeme == "}"){
            printOk(t);
            lexer->getNextToken();
        }
    }
    else{
        error("Opening curly braces expected {", t);
    }
}

void Parser::statement() {
    Token *t = lexer->peekNextToken();
    if(t->Lexeme == "}") {
        return;
    }

    else if(t->Lexeme == "var"){
        varDeclarStatement();
    }

    else if(t->Lexeme == "let"){
        letStatement();
    }

    else if(t->Lexeme == "if"){
        ifStatement();
    }

    else if(t->Lexeme == "while"){
        whileStatement();
    }

    else if(t->Lexeme == "do"){
        doStatement();
    }

    else if(t->Lexeme == "return"){
        returnStatement();
    }

    else{
        error("Statement is expected", t);
    }
}

void Parser::varDeclarStatement(){
    Token *t = lexer->getNextToken();

    if(t->Lexeme == "var"){
        string symbolType = lexer->peekNextToken()->Lexeme;
        type();
        t = lexer->getNextToken();
        if(t->Type == Token().Identifier){
            localVariablesCount++;
            //check the symbol table
            SymbolAlreadyDeclared(t);
            symbolTables.at(symbolTables.size() - 1)->AddSymbol(t->Lexeme,Symbol().var,symbolType);
            printOk(t);
            t = lexer->getNextToken();
            while(t->Lexeme == ",") {
                localVariablesCount++;
                t = lexer->getNextToken();
                if (t->Type == Token().Identifier) {
                    SymbolAlreadyDeclared(t);
                    symbolTables.at(symbolTables.size() - 1)->AddSymbol(t->Lexeme,Symbol().var,symbolType);
                    printOk(t);
                    t = lexer->getNextToken();
                }
                else {
                    error("Var declaration ends with , ", t);
                }
            }
        }
        else{
            error("Identifier expected ", t);
        }
        if(t->Lexeme == ";"){
            printOk(t);
        }
        else{
            error("Missing ; symbol", t);
        }
    }
    else{
        error("Var declaration expected", t);
    }
}

void Parser::letStatement(){
    Token *t = lexer->getNextToken();
    Symbol* lhs;
    if(t->Lexeme == "let"){
        printOk(t);
        t = lexer->getNextToken();
        if(t->Type == Token().Identifier){
            // checking if symbol is declared
            SymbolNotDeclared(t);
            printOk(t);
            // marking an identifier is initialized
            lhs = getSymbol(t);
            lhs->initialized = true;


            t = lexer->getNextToken();
            if(t->Lexeme == "["){
                t = lexer->peekNextToken();
                Symbol* s = getSymbol(t);
                if(t->Type != Token().Integer && (s != NULL && s->type != "int" && s->type != "Array"))
                    warning("Wrong array index",t);
                printOk(t);
                expression();
                t = lexer->getNextToken();
                if(t->Lexeme == "]"){
                    printOk(t);
                    t = lexer->getNextToken();
                }
                else{
                    error("Closing bracket missing ]", t);
                }
            }

            lhsType = lhs->type;
            if(t->Lexeme == "="){
                printOk(t);
                Token *temp = lexer->peekNextToken();
                expression();
                code.push_back("pop " + lhs->getSymbolOffset());
                t = lexer->getNextToken();
                if(t->Lexeme == ";"){
                    printOk(t);
                    lhsType = "";
                }
                else{
                    error("; expected ", t);
                }
            }
            else{
                error("= symbol expected", t);
            }
        }
        else{
            error("Identifier expected ", t);
        }
    }
    else{
        error("let expected ", t);
    }
}

void Parser::ifStatement() {
    // keeps track if if statement returns a value
    bool ifReturns = false;

    Token* t = lexer->getNextToken();
    if(t->Lexeme == "if"){
        t = lexer->getNextToken();
        if(t->Lexeme == "("){
            expression();
            t = lexer->getNextToken();
            if(t->Lexeme == ")"){
                code.push_back("if-goto IF_TRUE0");
                code.push_back("goto IF_FALSE0");
                printOk(t);
            }
            else{
                error("Closing brace expected", t);
            }
        }
        else{
            error("Opening bracket expected", t);
        }
        t = lexer->getNextToken();
        if(t->Lexeme == "{"){
            code.push_back("label IF_TRUE0");
            SymbolTable* ifTable = new SymbolTable();
            symbolTables.push_back(ifTable);
            t = lexer->peekNextToken();

            while (t->Lexeme != "}") {
                statement();
                t = lexer->peekNextToken();
            }
            code.push_back("label IF_FALSE0");
            symbolTables.pop_back();
            printOk(t);
            lexer->getNextToken();

            if(ifTable->FindSymbol("return")) {
                ifReturns = true;
            }
            delete(ifTable);

        }
        else{
           error("Curly bracket expected: {", t);
        }
        t = lexer->peekNextToken();
        if(t->Lexeme == "else"){
            lexer->getNextToken();
            printOk(t);
            t = lexer->getNextToken();
            if(t->Lexeme == "{"){
                SymbolTable* elseTable = new SymbolTable();

                symbolTables.push_back(elseTable);
                t = lexer->peekNextToken();

                while (t->Lexeme != "}") {
                    statement();
                    t = lexer->peekNextToken();
                }
                symbolTables.pop_back();
                printOk(t);

                if(ifReturns && elseTable->FindSymbol("return")){
                    symbolTables.back()->AddSymbol("return", Symbol().returns,"return");
                }
                delete(elseTable);

                lexer->getNextToken();
            }
        }
    }
}

void Parser::whileStatement() {
    Token* t = lexer->getNextToken();
    if(t->Lexeme == "while"){
        code.push_back("label WHILE_EXP0");
        printOk(t);
        t = lexer->getNextToken();
        if(t->Lexeme == "("){
            printOk(t);
            expression();
            t = lexer->getNextToken();
            if(t->Lexeme == ")"){
                printOk(t);
                t = lexer->getNextToken();
                if(t->Lexeme == "{"){
                    code.push_back("not");
                    code.push_back("if-goto WHILE_END0");
                    //creating new symbol table
                    symbolTables.push_back(new SymbolTable());
                    printOk(t);
                    t = lexer->peekNextToken();
                    while(t->Lexeme != "}"){
                        printOk(t);
                        statement();
                        t = lexer->peekNextToken();
                    }
                    code.push_back("goto WHILE_EXP0");
                    code.push_back("label WHILE_END0");
                    //removing the top symbol table
                    symbolTables.pop_back();
                    printOk(t);
                    lexer->getNextToken();
                }
                else{
                    error("Opening curly bracket expected ",t);
                }
            }
            else{
                error("Closing brace expected",t );
            }
        }
        else{
            error("Opening brace expected: (",t);
        }
    }
    else{
        error("While expression expected",t);
    }
}

void Parser::doStatement() {
    Token* t = lexer->getNextToken();
    if(t->Lexeme == "do"){
        printOk(t);
        subroutineCall();
        t = lexer->getNextToken();
        if(t->Lexeme == ";"){
            printOk(t);
        }
        else{
            error("; expected", t);
        }
    }
    else{
        error("Do keyword expected ", t);
    }
}

void Parser::subroutineCall() {
    Token* t = lexer->getNextToken();
    if(t->Type == Token().Identifier){
        // setting class name and subroutine
        string cName = t->Lexeme;
        Token* subroutine = NULL;
        printOk(t);

//        // pushing Identifier on top
//        Symbol* s = getSymbol(t);
//        if(s != NULL){
//            code.push_back("push " + s->getSymbolOffset());
//        }

        Token* temp = lexer->peekNextToken();
        if(temp->Lexeme == "."){
            t = lexer->getNextToken();
            printOk(t);
            t = lexer->getNextToken();
            if(t->Type == Token().Identifier){
                subroutine = t;
                printOk(t);
                t = lexer->getNextToken();
            }
            else{
                error("Identifier expected ", t);
            }
        }
        else{
            t = lexer->getNextToken();
            symbolInitialized(t);
        }
        if(t->Lexeme == "("){
            printOk(t);
            expressionList();
            t = lexer->getNextToken();
            if(t->Lexeme == ")"){
                printOk(t);
                // after expression list is called argsCount represents the number of arguments in the call
                // if subroutine is not null then subroutine from other class was called
                // and it needs to be checked later on
                if(subroutine != NULL){
                    if(getSymbolByName(cName) == NULL) {
                        addIdentifier(cName, subroutine->Lexeme, argsCount, subroutine);
                        code.push_back("call " + cName + "." + subroutine->Lexeme + " " + to_string(argsCount));
                    }
                    else {
                        addIdentifier(getSymbolByName(cName)->type, subroutine->Lexeme, argsCount, subroutine);
                        code.push_back("push " + getSymbolByName(cName)->getSymbolOffset());
                        code.push_back("call " + getSymbolByName(cName)->type + "." + subroutine->Lexeme + " " + to_string(argsCount+1));
                    }
                    code.push_back("pop temp 0");
                }
            }
            else{
                error("Closing bracket expected ", t);
            }
        }
        else{
            error("Opening bracket expected ", t);
        }


    }
    else{
        error("Identifier expected ", t);
    }
}

void Parser::expressionList() {
//    expressions count
    int args = 0;
    argsCount = 0;

    Token* t = lexer->peekNextToken();
    if(t->Lexeme == ")"){
        printOk(t);
        return;
    }
    else {
        ++args;
        ++argsCount;
        expression();
        t = lexer->peekNextToken();
        while (t->Lexeme == ",") {
            ++args;
            ++argsCount;
            lexer->getNextToken();
            expression();
            t = lexer->peekNextToken();
        }
    }
    argsCount = args;
}

void Parser::returnStatement() {
    Token* t = lexer->getNextToken();
    if(t->Lexeme == "return"){
        printOk(t);
        t = lexer->peekNextToken();
        Symbol* function = getSymbolByName("this");
        if(t->Lexeme != ";"){
            //checking return types
            if(t->Type == Token().Integer && function->type != "int"){
                warning("Return type does not mach the function",t);
            }
            else if(t->Type == Token().StringLiteral && function->type != "String"){
                warning("Return type does not mach the function",t);
            }
            else if(t->Lexeme == "true" || t->Lexeme == "false" && function->type != "bool"){
                warning("Return type does not mach the function",t);
            }
            else{
                Symbol *s = getSymbol(t);
                if(s != NULL && s->type != function->type && t->Lexeme != function->type){
                    warning("Return type does not mach the function",t);
                }
                else{

                }
            }
            expression();
            code.push_back("return");
        }
        else{
            if(function->type != "void"){
                error("Return value expected",t);
            }
            code.push_back("push constant 0");
            code.push_back("return");
        }
        t = lexer->getNextToken();
        if(t->Lexeme == ";"){
            printOk(t);
            SymbolTable* last = symbolTables.back();
            last->AddSymbol("return",Symbol().returns,"return");
        }
        else{
            error("; expected",t);
        }
    }
}

void Parser::expression() {
    relationalExpression();

    Token* t = lexer->peekNextToken();
    while(t->Lexeme == "&" || t->Lexeme == "|"){
        lexer->getNextToken();
        relationalExpression();
        if(t->Lexeme == "&"){
            code.push_back("and");
        }
        else
            code.push_back("or");
        t = lexer->peekNextToken();
    }
}

void Parser::relationalExpression() {
    ArithmeticExpression();
    Token *t = lexer->peekNextToken();
    while(t->Lexeme == "=" || t->Lexeme == ">" || t->Lexeme == "<"){
        printOk(t);
        lexer->getNextToken();
        ArithmeticExpression();
        if(t->Lexeme == "="){
            code.push_back("eq");
        }
        else if(t->Lexeme == ">"){
            code.push_back("gt");
        }
        else if(t->Lexeme == "<"){
            code.push_back("lt");
        }
        t = lexer->peekNextToken();
    }
}

void Parser::ArithmeticExpression() {
    term();
    Token *t = lexer->peekNextToken();
    while(t->Lexeme == "+" || t->Lexeme == "-"){
        printOk(t);
        lexer->getNextToken();
        term();
        if(t->Lexeme == "+")
            code.push_back("add");
        else
            code.push_back("sub");

        t = lexer->peekNextToken();
    }
}

void Parser::term() {
    factor();
    Token *t = lexer->peekNextToken();
    while(t->Lexeme == "*" || t->Lexeme == "/"){
        printOk(t);
        lexer->getNextToken();
        factor();
        if(t->Lexeme == "*")
            code.push_back("call Math.multiply 2");
        else
            code.push_back("call Math.divide 2");
        t = lexer->peekNextToken();
    }
}

void Parser::factor() {
    Token* t = lexer->peekNextToken();
    if(t->Lexeme == "-" || t->Lexeme == "~"){
        lexer->getNextToken();
    }
    operand();
    if(t->Lexeme == "-"){
        code.push_back("neg");
    }
    else if(t->Lexeme == "~"){
        code.push_back("not");
    }
}

void Parser::operand() {
    Token* t = lexer->getNextToken();
    if(t->Type == Token().Integer){
        //If integer is encounter then we need to check it with the lhs
        //If types do not match then a warning is thrown
        if(lhsType != "" && lhsType != "int" && lhsType != "char"  && lhsType != "boolean"  && lhsType != "String" && lhsType != "Array"){
            warning("LHS and RHS types do not match",t);
        }
        code.push_back("push constant " + t->Lexeme);
        printOk(t);
    }

    else if(t->Type == Token().Identifier){
        //setting class name and subroutine pointer that are going to be used later
        //when adding to identifiers to check table
        string cName = t->Lexeme;
        Token* subroutine = NULL;

        Token* temp = lexer->peekNextToken();
        Symbol* s = getSymbol(t);
        bool flag = true;
        if(temp->Lexeme == "."){
            lhsType = "";
            printOk(t);
            printOk(temp);
            lexer->getNextToken();
            t = lexer->getNextToken();
            if(t->Type == Token().Identifier){
                subroutine = t;
                printOk(t);
                t = lexer->peekNextToken();
                if(t->Lexeme == "["){
                    lexer->getNextToken();
                    printOk(t);
                    expression();
                    t = lexer->getNextToken();
                    if(t->Lexeme == "]"){
                        // adding to identifier table in order to check if it is declared
                        addIdentifier(cName,subroutine->Lexeme,-1,subroutine);
                        printOk(t);
                    }
                    else{
                        error("Closing bracket expected", t);
                    }
                }
                else if(t->Lexeme == "("){
                    lexer->getNextToken();
                    printOk(t);
                    expressionList();
                    t = lexer->getNextToken();
                    if(t->Lexeme == ")"){
                        flag = false;
                        // adding to identifier table in order to check if it is declared
                        // if symbol exists then function is called on an identifier and
                        // class name is identifier's type
                        // otherwise class name is cName
                        if(getSymbolByName(cName) == NULL) {
                            addIdentifier(cName, subroutine->Lexeme, argsCount, subroutine);
                            code.push_back("call " + cName + "." + subroutine->Lexeme + " " + to_string(argsCount));
                        }
                        else {
                            addIdentifier(getSymbolByName(cName)->type, subroutine->Lexeme, argsCount, subroutine);
                            code.push_back("push " + getSymbolByName(cName)->getSymbolOffset());
                            code.push_back("call " + getSymbolByName(cName)->type + "." + subroutine->Lexeme + " " + to_string(argsCount+1));
                            //code.push_back("pop temp 0");
                        }
                        printOk(t);
                    }
                    else{
                        error("Closing brace expected", t);
                    }
                }
                else{
                    // in this case identifier is not a subroutine but a variable
                    // thus args is -1
                    addIdentifier(cName,subroutine->Lexeme,-1,subroutine);
                }
            }
            else{
                error("Identifier expected", t);
            }
        }
        else{
            SymbolNotDeclared(t);
            symbolInitialized(t);

            // checking if lhs type matches.
            if(s != NULL && lhsType != "" && lhsType != s->type && s->type != "Array" && lhsType != "Array"
            && (s != NULL && lhsType != "" &&  s->type == "int" && lhsType != "boolean" && lhsType != "String" && lhsType != "char")){
                warning("LHS and RHS types do not match",t);
            }
            printOk(t);
            t = lexer->peekNextToken();
        }
        if(t->Lexeme == "["){
            printOk(t);
            lexer->getNextToken();
            expression();
            t = lexer->getNextToken();
            if(t->Lexeme == "]"){
                printOk(t);
            }
            else{
                error("Closing bracket expected", t);
            }
        }
        else if(t->Lexeme == "("){
            printOk(t);
            expressionList();
            t = lexer->getNextToken();
            if(t->Lexeme == ")"){
                code.push_back("call " + cName + " " + to_string(argsCount));
                printOk(t);
            }
            else{
                error("Closing brace expected", t);
            }
        }
        else{
            if(flag && s != NULL && (s->Kind == Symbol().var || s->Kind == Symbol().argument || s->Kind == Symbol().stat || s->Kind == Symbol().field))
                code.push_back("push " + s->getSymbolOffset());
        }
    }

    else if(t->Lexeme == "("){
        printOk(t);
        expression();
        t = lexer->getNextToken();
        if(t->Lexeme == ")"){
            printOk(t);
        }
        else{
            error("Closing brace expected", t);
        }
    }

    else if(t->Type == Token().StringLiteral){
        printOk(t);
    }

    else if(t->Lexeme == "true" || t->Lexeme == "false"){
        if(t->Lexeme == "true") {
            code.push_back("push constant 1");
            code.push_back("neg");
        }
        else
            code.push_back("push constant 0");
        printOk(t);
    }

    else if(t->Lexeme == "null"){
        code.push_back("push constant 0");
        printOk(t);
    }

    else if(t->Lexeme == "this"){
        code.push_back("push pointer 0");
        printOk(t);
    }

    else{
        error("Operand expected", t);
    }
}