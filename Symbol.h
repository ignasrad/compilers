//
// Created by Ignas on 3/17/2020.
//

#ifndef UNTITLED1_SYMBOL_H
#define UNTITLED1_SYMBOL_H

#include <string>

class Symbol {

public:
    std::string getSymbolOffset();
    enum SymbolKind{
        var,
        argument,
        stat,
        field,
        classKind,
        subroutine,
        returns
    };

    SymbolKind Kind;
    std::string name;
    std::string type;
    int offset;
    int arguments = 0;
    bool initialized = false;
};

#endif //UNTITLED1_SYMBOL_H
