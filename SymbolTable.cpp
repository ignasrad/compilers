//
// Created by Ignas on 3/17/2020.
//

#include <iostream>
#include "SymbolTable.h"

using namespace std;
void SymbolTable::AddSymbol(string name, Symbol::SymbolKind kind, string type) {
    Symbol *temp = new Symbol();
    temp->name = name;
    temp->Kind = kind;
    temp->type = type;

    if(kind == Symbol().argument)
        temp->offset = argCount++;
    else if(kind == Symbol().var)
        temp->offset = varCount++;
    else if(kind == Symbol().stat)
        temp->offset = staticCount++;
    else if(kind == Symbol().field)
        temp->offset = fieldCount++;

    table.push_back(temp);
}

bool SymbolTable::FindSymbol(string name) {
    for(int i=0; i<table.size(); i++){
        if(table.at(i)->name == name){
            return true;
        }
    }
    return false;
}

Symbol* SymbolTable::getSymbol(string name) {
    for(int i=0; i<table.size(); i++){
        if(table.at(i)->name == name){
            return table.at(i);
        }
    }
    return NULL;
}

