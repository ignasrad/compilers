//
// Created by sc18ir on 12/02/20.
//

#ifndef COMPILER_LEXER_H
#define COMPILER_LEXER_H

#include <vector>
#include "fstream"
#include <iostream>
#include "Token.h"
#include <dirent.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>


using namespace std;
class Lexer {

public:
    void openDirectory(string fileName);
    bool openFile();
    bool init(string fileName);
    Token* getNextToken();
    Token* peekNextToken();
    int getLineNr(){return lineNr;};
    string getFilename(){return fileName;};

private:
    string keywords [21] = {"class",
                       "constructor",
                       "method",
                       "function",
                       "int",
                       "boolean",
                       "char",
                       "void",
                       "var",
                       "static",
                       "field",
                       "let",
                       "do",
                       "if",
                       "else",
                       "while",
                       "return",
                       "true",
                       "false",
                       "null",
                       "this"};
    //returns next valid char, removes whitespace, new line symbols
    int getNext();
    Token *peekedToken;
    string fileName;
    ifstream file;
    int lineNr;
    DIR *dp;
    string dir;
};


#endif //COMPILER_LEXER_H
