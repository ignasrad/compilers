#include <iostream>
#include "Parser.h"



int main(int argc, char **argv) {

    if(argc < 2){
        cout<<"Please enter .jack program path"<<endl;
        return 1;
    }

    //string dir = "/home/ignas/Desktop/Jack Programs/Set 1/List";

    string filepath = argv[1];
    Parser *parser = new Parser();
    parser->init(filepath);


    return 0;
}