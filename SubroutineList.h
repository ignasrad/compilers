//
// Created by ignas on 2020-05-09.
//

#ifndef COMPILER_SUBROUTINELIST_H
#define COMPILER_SUBROUTINELIST_H

#include "Token.h"
#include <string>
#include <unordered_map>

using namespace std;
// a data structure to store identifiers from other classes and check them later on
class SubroutineList {

public:
    SubroutineList(string name, string file){className = name; fileName = file;}
    void warning(string message, Token* t);
    //    adds a new identifier and number of args
    //    if it is a variable, then args = -1 else it is the number of args
    void addIdentifier(string name, int args, Token* t);

    //    removes identifier from list if args number is the same,
    //    else returns false and throws a warning
    bool markIdentifier(string name, int args, string type);
    //    checks if everything is declared and throws warnings if not
    void isEverytingDeclared();

private:
    unordered_map<string,pair<int,Token*>> identifiers;
    string className = "";
    string fileName = "";
};


#endif //COMPILER_SUBROUTINELIST_H
