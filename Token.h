//
// Created by sc18ir on 12/02/20.
//

#ifndef COMPILER_TOKEN_H
#define COMPILER_TOKEN_H

#include <string>

class Token {

public:
    enum TokenTypes{
        Keyword,
        Identifier,
        Symbol,
        Integer,
        Boolean,
        Eof,
        StringLiteral};

    TokenTypes Type;
    std::string Lexeme;
    int lineNumber;

    Token(){
        Lexeme = "";
    }
};


#endif //COMPILER_TOKEN_H
