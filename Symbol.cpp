//
// Created by ignas on 2020-05-10.
//
#include "Symbol.h"

using namespace std;
string Symbol::getSymbolOffset(){
    if(Kind == 0){
        return "local " + to_string(offset);
    }
    if(Kind == 1){
        return "argument " + to_string(offset);
    }
    if(Kind == 2){
        return "static " + to_string(offset);
    }
    if(Kind == 3){
        return "this " +  to_string(offset);
    }
    return name;
}