//
// Created by sc18ir on 03/03/20.
//

#ifndef COMPILER_PARSER_H
#define COMPILER_PARSER_H

#include "Lexer.h"
#include "Token.h"
#include "SymbolTable.h"
#include "SubroutineList.h"

class Parser {

public:
    void init(string fileName);
    void classDeclar();

private:
    string defaultLibraries[8] = {"Math",
                               "Memory",
                               "Screen",
                               "Output",
                               "Keyboard",
                               "String",
                               "Array",
                               "Sys"};

    Lexer *lexer;
    vector<SymbolTable*> symbolTables;
    vector<string> code;
    unordered_map<string, SubroutineList*> identifiersToCheck;
    string className;
    string lhsType = "";
    int argsCount, localVariablesCount, fieldCount = 0;

    //    Functions to manipulate identifiers table
    void addIdentifier(string className, string idName, int args, Token *t);
    void markIdentifier(string idName, int args, string type);
    //    Throws a warning if any of identifiers haven't been matched
    //    i.e. class Y does not contain function or variable x
    void checkIfIdentifiersMatch();
    //    Creates a list of predefined libraries
    void createPredefinedLibraries();


    void error(string message, Token* t);
    void warning(string message, Token* t);
    void printOk(Token* t);
    //checks if variable is already taken and prints error if it is
    void SymbolAlreadyDeclared(Token* t);
    //checks if variable is declared and throws error if it isn't
    void SymbolNotDeclared(Token* t);
    Symbol* getSymbol(Token* t);
    Symbol* getSymbolByName(string s);
    void symbolInitialized(Token *t);
    void addCodeSeg(string ln);

    // code generation
    void writeCode(string filePath);

    //  jack language grammar expressions
    void memberDeclar();
    void classVarDeclar();
    void type();
    void subroutineDeclar();
    void paramList();
    void subroutineBody();
    void statement();
    void varDeclarStatement();
    void letStatement();
    void ifStatement();
    void whileStatement();
    void doStatement();
    void subroutineCall();
    void expressionList();
    void returnStatement();
    void expression();
    void relationalExpression();
    void ArithmeticExpression();
    void term();
    void factor();
    void operand();

};


#endif //COMPILER_PARSER_H
