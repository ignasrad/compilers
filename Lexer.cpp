//
// Created by sc18ir on 12/02/20.
//


#include "Lexer.h"
#include <iostream>
#include "Lexer.h"
#include "Parser.h"
#include <sys/types.h>
#include <dirent.h>

#include <string.h>

using namespace std;

void Lexer::openDirectory(string fileDir){

    if((dp = opendir(fileDir.c_str())) == NULL) {
        cout << "Error(" << errno << ") opening " << fileDir << endl;
        return;
    }
    else{
        dir = fileDir;
    }
}

bool Lexer::openFile(){
    string filepath;
    struct dirent *dirp;


    if ((dirp = readdir( dp )))
    {


        filepath = dir + "/" + dirp->d_name;

        // If the file is a directory (or is in some way invalid) we'll skip it
        if (filepath.substr(filepath.length()-5) != ".jack"){
            return openFile();
        }

        file.close();
        file.open(filepath);
        fileName = dirp->d_name;
        lineNr = 1;
        cout<<"Opened: "<<filepath<<endl;
        return true;
    }
    else{
        closedir(dp);
        return false;
    }
}

bool Lexer::init(string fileName){
    openDirectory(fileName);
    openFile();

    if(!file.is_open()) {
        cout << "File " << fileName << " does not exists" << endl;
        return false;
    }
    return true;
}

//returns next token
Token* Lexer::getNextToken() {
    Token *token = new Token();

    if(peekedToken != nullptr){
        token = peekedToken;
        peekedToken = nullptr;
        return token;
    }

    int c;

    c = getNext();



    //removing one line comments
    while(((char)c == '/' && (char)file.peek() == '/' && c != -1) || ((char)c == '\n' && c != -1)){
        while(c != '\n' && c != -1){
            c = file.get();
        }

        //increase line counter if line comment finished
        if(c == '\n')
            lineNr++;

        c = getNext();
    }

    //multi line and api comments
    while((char)c == '/' && (char)file.peek() == '*' && c != -1){
        while( ((char)c != '*' || (char)file.peek() != '/') && c != -1){
            if((char)c == '\n')
                lineNr++;
            c = file.get();
        }
        getNext();
        c = getNext();
    }

    //checking if it's a string literal
    if(c == '"'){
        string str = "\"";
        c = file.get();
        while(c != '"' && c != -1){
            if((char)c == '\n')
                lineNr++;
            str += c;
            c = file.get();
        }
        str += "\"";
        token->Lexeme = str;
        token->Type = Token().StringLiteral;
        token->lineNumber = lineNr;
        return token;
    }

    //retrieve keyword or identifier
    string str = "";
    if(isalpha(c) || (char)c == '_') {
        while (isalpha((char)c) ||  (char)c == '_' || isdigit((char)c)) {
            str += c;
            c = file.get();
        }

        //making sure that pointer now points to the last symbol of keyword or id
        //in case for example "var something;" so semicolon would be read next time
        file.unget();
        token->Lexeme = str;
        token->lineNumber = lineNr;
        for(int i=0; i < sizeof(keywords)/sizeof(keywords[0]); i++){
            if(keywords[i].compare(str) == 0){
                token->Type = Token().Keyword;
                return token;
            }
        }

        if(str == "true" || str == "false"){
            token->Type = Token().Boolean;
            return token;
        }

        token->Type = Token().Identifier;
        return token;
    }

    str = "";
    if(isdigit((char)c)){
        while(isdigit((char)c)) {
            str += c;
            c = file.get();
        }

        file.unget();
        token->Lexeme = str;
        token->lineNumber = lineNr;
        token->Type = Token().Integer;
        return token;
    }


    //checking if eof is reached
    if(c == -1){
        token->Type = Token().Eof;
        token->lineNumber = lineNr;
        if(openFile())
            return getNextToken();
        else {
            file.close();
            return token;
        }
    }

    //if none of the above then it must be a symbol
    token->Lexeme = c;
    token->lineNumber = lineNr;
    token->Type = Token().Symbol;

    return token;
}

Token* Lexer::peekNextToken() {
    if(peekedToken == nullptr){
        peekedToken = getNextToken();
    }
    return peekedToken;
}

//skips white space and returns next char
int Lexer::getNext() {
    int c = file.get();

    //checking for unexpected eof symbol
    if(c == -1)
        return -1;

    while(c != -1 && ((char)c == ' ' || (char)c == '\n' || c == 9 || c == 13)){
        if((char)c == '\n')
            lineNr++;
        c = file.get();
    }
    return c;
}